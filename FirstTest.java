import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class FirstTest {

    @Test
    public void checkRozetkaIsOpened() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://rozetka.com.ua/");
        WebElement element = driver.findElement(By.xpath("//img[@alt='Rozetka Logo']"));
        boolean result = element.isDisplayed();
        System.out.println(result);
        Assert.assertTrue(result, "Rozetka is opened");

        driver.quit();
    }

    @Test
    public void CheckClearButtonAppeared() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://rozetka.com.ua/");
        WebElement search = driver.findElement(By.xpath("//input[@name='search']"));
        search.sendKeys("Стол");
        WebElement clearButton = driver.findElement(By.xpath("//button[@aria-label='Очистить поиск' or @aria-label='Очистити пошук']"));
        boolean result = clearButton.isDisplayed();
        Assert.assertTrue(result, "Clear button successfully appeared");

        driver.quit();
    }

    @Test
    public void CheckAuthForm() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");

        WebElement authorizeButton = driver.findElement(By.xpath("//*[contains(@class,'header-actions__item--user')]//button"));
        authorizeButton.click();

        WebElement authForm = driver.findElement(By.xpath("//form[contains(@class,'auth-modal__form')]"));

        boolean result = authForm.isDisplayed();
        Assert.assertTrue(result, "AuthForm is opened");

        driver.quit();
    }

    @Test
    public void findTableOnRozetka() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
       // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        WebElement element = driver.findElement(By.xpath("//input[@name='search']"));
        element.sendKeys("стол" + Keys.ENTER);

        WebElement officeTable = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(),'Стол для настольного тенниса Donic Indoor Roller Fun Blue')]")));

        //WebElement officeTable = driver.findElement(By.xpath("//*[contains(text(),'Стол для настольного тенниса Donic Indoor Roller Fun Blue')]"));
        officeTable.click();


        driver.quit();
    }

    @Test
    public void OrderTableOnRozetka() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");

        WebElement element = driver.findElement(By.xpath("//input[@name='search']"));
        element.sendKeys("стол" + Keys.ENTER);

        WebElement officeTable = driver.findElement(By.xpath("//*[contains(text(),'Стол для настольного тенниса Donic Indoor Roller Fun Blue')]"));
        officeTable.click();

        WebElement buyProduct = driver.findElement(By.xpath("//button[(@aria-label='Купити' or @aria-label='Купить') and contains(@class,'button--green')]"));
        buyProduct.click();

        WebElement OrderProduct = driver.findElement(By.xpath("//a[@data-testid='cart-receipt-submit-order']"));

        boolean result = OrderProduct.isDisplayed();
        Assert.assertTrue(result, "Order button is displayed");

        driver.quit();
    }

    @Test
    public void BuyTableOnRozetka() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");

        WebElement element = driver.findElement(By.xpath("//input[@name='search']"));
        element.sendKeys("стол" + Keys.ENTER);

        WebElement officeTable = driver.findElement(By.xpath("//*[contains(text(),'Стол для настольного тенниса Donic Indoor Roller Fun Blue')]"));
        officeTable.click();

        WebElement buyProduct = driver.findElement(By.xpath("//button[(@aria-label='Купити' or @aria-label='Купить') and contains(@class,'button--green')]"));
        buyProduct.click();

        WebElement OrderProduct = driver.findElement(By.xpath("//a[@data-testid='cart-receipt-submit-order']"));
        OrderProduct.click();

        WebElement BuyProduct = driver.findElement(By.xpath("//input[contains(@class,'checkout-total__submit')]"));


        boolean result = BuyProduct.isDisplayed();
        Assert.assertTrue(result, "Buy button is displayed");

        driver.quit();
    }

    @Test
    public void InputSurname() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");

        WebElement element = driver.findElement(By.xpath("//input[@name='search']"));
        element.sendKeys("стол" + Keys.ENTER);

        WebElement officeTable = driver.findElement(By.xpath("//*[contains(text(),'Стол для настольного тенниса Donic Indoor Roller Fun Blue')]"));
        officeTable.click();

        WebElement buyProduct = driver.findElement(By.xpath("//button[(@aria-label='Купити' or @aria-label='Купить') and contains(@class,'button--green')]"));
        buyProduct.click();

        WebElement OrderProduct = driver.findElement(By.xpath("//a[@data-testid='cart-receipt-submit-order']"));
        OrderProduct.click();

        WebElement InputSurname = driver.findElement(By.xpath("//input[@formcontrolname='surname' and @class='ng-untouched ng-pristine ng-invalid']"));
        InputSurname.sendKeys("Vadym");


        driver.quit();
    }

    @Test
    public void openRozetkaYoutube() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        WebElement youtubeButton = driver.findElement(By.xpath("//a[@class='socials__link socials__link--youtube']"));
        youtubeButton.click();


        driver.quit();
    }

    @Test
    public void openMenu() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        WebElement menuButton = driver.findElement(By.xpath("//button[@aria-label='Открыть меню' or @aria-label='Відкрити меню']"));
        menuButton.click();

        WebElement sideMenu = driver.findElement(By.xpath("//div[contains(@class,'side-menu ')]"));
        boolean result = sideMenu.isDisplayed();
        Assert.assertTrue(result, "Side menu is opened");

        driver.quit();
    }

    @Test
    public void closeBanner() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        WebElement bannerCloseButton = driver.findElement(By.xpath("//*[@id='rz-banner']/span"));
        bannerCloseButton.click();

        driver.quit();
    }

}